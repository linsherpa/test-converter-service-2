from flask import send_file, send_from_directory
from apiflask import APIFlask, Schema, abort
from apiflask.fields import Integer, String, File
from apiflask.validators import Length, OneOf
# from marshmallow import Schema, fields
from werkzeug.utils import secure_filename
import os
from open_api.prep_file_api import preparation
from open_api.conversion_api import conversion_stage
from open_api.validation_api import validation_stage

# from api_scripts.loading_animation import Loader


#main_path = "/home/lincoln/Documents/del_api_flask/apiflask_library"
main_path = os.getcwd()
temp_file_folder = os.path.join(main_path, "input_files")

app = APIFlask(__name__, title='For testing', version='1.0', docs_path='/api-docs')

app.config['DESCRIPTION'] = "This instance is for generating rest-api for the converter service"
app.config['EXTERNAL_DOCS'] = {
    'description': "This is a test version of MS data converstion",
    'url': 'JustForATestTrial'
}

app.config['TERMS_OF_SERVICE'] = 'http://localhost:5000'
app.config['LICENSE'] = {
    'name': 'MIT',
    'url': 'https://opensource.org/licenses/MIT'
}

app.config['TAGS'] = [
    {'name': 'Conversion',
     'description': 'The aim of conversion is to convert a format to a standardized/agreed format'},
    {'name': 'Validation',
     'description': 'The goal of validation is to Validate the converted format converted by converter service.'}
]

app.config['IMAGE_FOLDER'] = 'input_files'


class Files_convert(Schema):
    main_zip_file = File(
        required=True,
        metadata={'title': 'The path of the zipped file',
                  'description': 'Please add the zipped MassHunter.D Folder'}
    )
    additional_info = String(
        required=False,
        metadata={'title': 'This is for additional parameter that could be added for conversion',
                  'description': 'Additional parameter that can modify the outputs'}
    )


class Files_validate(Schema):
    Validation_file = File(
        required=False,
        metadata={'title': 'The path of the mzML file',
                  'description': 'The path of the mzML file is given'}
    )
    test = String(
        required=False,
        metadata={'title': 'For conversion of convert_file',
                  'description': 'The conversion takes any convert_raw files and then converts to mzML'}
    )


class File_download(Schema):
    meta = String(
        required=False,
        metadata={'title': 'For downloading the files generated by the Files_convert',
                  'Description': 'Downloading of the file'}
    )


class File_download2(Schema):
    meta = String(
        required=False,
        metadata={'title': 'Download of report file generated by the validation',
                  'Description': 'Downloading of the file'}
    )


@app.post('/conversion_file')
@app.input(Files_convert, location='form_and_files')
@app.doc(tags=['Conversion'])
def raw_conversion(form_and_files_data):
    #print(f'The folder path beforehand is {main_path}')
    file_t = form_and_files_data['main_zip_file']
    filename = secure_filename(file_t.filename)
    print(f'The Filename is : {filename}')
    test = preparation()
    test.cleanup_files()
    #print(f'{temp_file_folder} and {filename}')
    file_t.save(os.path.join(temp_file_folder, filename))
    #test = preparation()
    #test.current_path()
    test.create_intermediate_files()
    os.chdir(main_path)
    test2 = conversion_stage()
    #test2.current_path()
    test2.create_bash()
    os.chdir(main_path)
    download_converted_file()
    # os.chdir(main_path)
    #test2.cleanup()
    return {'message': 'Upload Success',
            'name of file': f'{filename}',
            'another_message': f'just for test'
            }


@app.get('/conversion_file')
@app.output(File_download)
@app.doc(tags=['Conversion'])
def download_converted_file():
    for file in os.listdir(temp_file_folder):
        if ".mzML" in file:
            return send_from_directory("input_files", file, as_attachment=True)
    print("Download Complete")
    return {'message': 'Download Completed',
            'task': 'Conversion of File to mzML'}


@app.post('/validation_file')
@app.input(Files_validate, location='form_and_files')
@app.doc(tags=['Validation'])
def validation_result(form_and_files_data):
    #print(f'The folder path beforehand is {main_path}')
    file_t = form_and_files_data['Validation_file']
    filename = secure_filename(file_t.filename)
    test = validation_stage()
    test.cleanup_old_files()
    file_t.save(os.path.join(temp_file_folder, filename))
    #print(f'The folder path ATM is {main_path}')
    #test = validation_stage()
    test.validation_process()
    os.chdir(main_path)
    for file in os.listdir("input_files"):
        if "_validation_result.txt" in file:
            print(f'validation Report Generated for file: {filename}')
            with open(os.path.join("input_files", file), 'r') as f_in:
                data = f_in.read()
            print(data)
            # download_validation_result()
    #print(f'path of the folder is : {main_path}')
    # os.chdir(main_path)
    # test2.cleanup()
    return {'message': 'Upload Success',
            'name of file': f'{filename}',
            'another_message': f'just for test'
            }


@app.get('/validation_file')
@app.output(File_download2)
@app.doc(tags=['Validation'])
def download_validation_result():
    for file in os.listdir(temp_file_folder):
        if "_validation_result.txt" in file:
            return send_from_directory(app.config['IMAGE_FOLDER'], file)
    print("Download Complete")
    return {'message': 'Download Completed',
            'task': 'Conversion of File to mzML'}


if __name__ == "__main__":
    os.chdir(main_path)
    app.run(host='0.0.0.0', debug=True)
