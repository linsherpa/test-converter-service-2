#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow
label: For conversion purpose

inputs:
  in_file:
    type: Directory

  parameters:
    type: string[]
    default: [mzML]

steps:
  step1:
    run: string_extractor.cwl
    in:
      input_file: in_file
    out: [output2, output3, output4]

  step2:
    run: msconvert.cwl
    in:
      in_dir: in_file
      in_string: step1/output2
      in_string2: step1/output3
      in_string3: step1/output4
    out: [outfile, output_dir, output_string]

  step3:
    run: validation_file_creation.cwl
    in:
      input: step1/output3
      in_dir: step1/output2
    out: [output_file, output_dir]

outputs:
  outputA:
    type: string
    outputSource: step1/output2

  outputB:
    type: File
    outputSource: step2/outfile

  outputC:
    type: Directory
    outputSource: step3/output_dir


$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
  - https://schema.org/version/latest/schemaorg-current-http.rdf
  - http://edamontology.org/EDAM_1.18.owl
