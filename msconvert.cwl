#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: Uses Proteowizard MSConvert to convert vendor files into mzML

baseCommand: ["wine64_anyuser", "msconvert"]

requirements:
  DockerRequirement:
    dockerPull: chambm/pwiz-skyline-i-agree-to-the-vendor-licenses
  InitialWorkDirRequirement:
    listing:
      - entry: $(inputs.in_dir)
        writable: true
  InlineJavascriptRequirement: {}

inputs:
  in_dir:
    type: Directory
    inputBinding:
      position: 1

  in_string:
    type: string

  in_string2:
    type: string

  in_string3:
    type: string

  parameters:
    type: string[]
    default: [mzML]
    inputBinding:
      position: 2

outputs:
  outfile:
    type: File
    label: mzML file
    format: edam:format_3245
    outputBinding:
      glob: $(inputs.in_string2).mzML

  output_string:
    type: string
    outputBinding:
      outputEval: $(inputs.in_string)

  output_dir:
    type: Directory
    outputBinding:
      glob: .
      outputEval: |
        ${
         self[0].basename = inputs.in_string;
         return self[0];
         }

# stdout: $(inputs.in_string).mzML

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
  - https://schema.org/version/latest/schemaorg-current-http.rdf
  - http://edamontology.org/EDAM_1.18.owl
