import os
import shutil

## Requirements
##

# Changing into Working directory

os.path.dirname(os.getcwd())
main_dir = os.getcwd()

sub_dir = "input_files"
int_dir = "intermediate_files_"
int_path = os.path.join(main_dir, int_dir)
work_path = os.path.join(main_dir, sub_dir)
os.chdir(main_dir)


class preparation:
    def __int__(self):
        self.root = None


    def cleanup_files(self):
        for file in os.listdir(int_path):
            if os.path.isfile(os.path.join(int_path, file)):
                os.remove(os.path.join(int_path, file))
            if os.path.isdir(os.path.join(int_path, file)):
                shutil.rmtree(os.path.join(int_path, file))

        for file in os.listdir(work_path):
            if os.path.isfile(os.path.join(work_path, file)):
                os.remove(os.path.join(work_path, file))
            if os.path.isdir(os.path.join(work_path, file)):
                shutil.rmtree(os.path.join(work_path, file))
        os.chdir(main_dir)

    def unzip_folder(self):
        for file in os.listdir(work_path):
            if ".zip" in file:
                shutil.unpack_archive(os.path.join(work_path, file), work_path)
                os.remove(os.path.join(work_path, file))

    def create_intermediate_files(self):
        self.unzip_folder()
        os.chdir(work_path)
        for file in os.listdir(os.getcwd()):
            if (os.path.isfile(file) or os.path.isdir(file)) and file != ".gitkeep":
                tmp_file_basename = os.path.splitext(file)[0]
                # creating an intermediate yml file for conversion
                os.chdir(main_dir)
                tmp_intermediate_file = "intermediate_files_/msconvert_workflow_file_" + tmp_file_basename + ".yml"
                #file_location = os.path.join(work_path, file)
                with open(tmp_intermediate_file, 'w') as g:
                    g.write('in_file:\n    class: Directory\n    format: http://edamontology.org/format_3245\n')
                    g.write(f'    path: {os.path.join(work_path, file)}')
                print(f'The Intermediate file Completed: {file} ')
                os.chdir(work_path)
                # print(f'The name of the file is {os.path.splitext(file)[0]}')
            else:
                print(f'Skipping the file: {file}')

        os.chdir(main_dir)


#if __name__ == "__main__":
#    test = preparation()
#    test.create_intermediate_files()
#    os.chdir(main_dir)