import os
import time
from subprocess import PIPE, run
import shutil

#
# Setting up the working Directory Path
#
main_path = os.getcwd()
work_path = "input_files"
working_path = os.path.join(main_path, work_path)
sec_path = "intermediate_files_"
cwl_path = "msconvert_workflow_main.cwl"
yml_path = os.path.join(main_path, sec_path)
os.chdir(main_path)


class conversion_stage:
    def __init__(self):
        self.result = None
        self.file = None

    # creating a bash script for  conversion-mzML
    def create_bash(self):
        for self.file in os.listdir(os.path.join(main_path, sec_path)):
            file_name = self.file.replace("msconvert_workflow_file_", "")
            bash_file_name = "cwl_exec_" + os.path.splitext(file_name)[0] + ".sh"
            with open(bash_file_name, "w") as f:
                f.write('#!bin/bash\n')
                f.write(f'\ncwltool --no-read-only {cwl_path} {os.path.join(yml_path, self.file)}')

            start_time = time.time()
            self.result = run(["sh", bash_file_name], stdout=PIPE, stderr=PIPE, universal_newlines=True)
            end_time = time.time()
            diff_time = end_time - start_time
            print(f'\nTime taken by server: {diff_time}sec')
            if self.result.stdout:
                print(f'Standard Output:\n---------------------\n {self.result.stdout}')

            if "error" in self.result.stderr.lower():
                print(f'Standard Error:\n---------------------\n {self.result.stderr}')
                print(f'mzML conversion of the file\n{self.file} ::--> fail')
            else:
                print(f'mzML conversion of the file\n{self.file} ::--> success')

            os.remove(bash_file_name)
        self.add_mzml()

    def add_mzml(self):
        for file in os.listdir(main_path):
            if ".mzML" in file:
                shutil.move(os.path.join(main_path, file), working_path)
        os.chdir(main_path)


#if __name__ == "__main__":
#    test = conversion_stage()
#    test.create_bash()
#    os.chdir(main_path)