import os
from subprocess import PIPE, run
import time
#
# Change to the working directory path
#
os.path.dirname(os.getcwd())
main_path = os.getcwd()
sec_path = "example_file"
folder_path = os.path.join(main_path, sec_path)
conversion_path = "example_file/mzml_conversion"
validation_path = "example_file/mzml_validation"
os.chdir(main_path)

class validation_stage:
    def __init__(self):
        self.result = None
        self.folder = None
        self.file = None
        self.filepath = None
    def validation_process(self):
        for self.folder in os.listdir(os.path.join(main_path, sec_path)):
            if self.folder != 'mzml_conversion' and self.folder != 'mzml_validation' and self.folder !='.gitkeep':
                #print(self.folder)
                # Creating files for validation of File
                for file in os.listdir(os.path.join(folder_path, self.folder)):
                    if "_validation_file.yml" in file and file != "_validation_file.yml":
                        #self.filepath = os.path.abspath(file)
                        self.filepath = os.path.join(os.path.join(folder_path, self.folder), file)
                        os.chdir(main_path)
                        file_name = file.split("_validation_file.yml")[0]
                        bash_file_name = "validation_exec_" + file_name + ".sh"
                        with open(bash_file_name, "w") as f:
                            f.write('#!bin/bash\n')
                            f.write(f'\ncwltool validation.cwl {self.filepath}')
                        start_time = time.time()
                        self.result = run(["sh", bash_file_name], stdout=PIPE, stderr=PIPE, universal_newlines=True)
                        end_time = time.time()
                        diff_time = end_time -  start_time
                        #if "Failed" in self.result.stderr:
                        tmp_error_report = file_name + "_mzML_validation_op.txt"
                        standard_error_report = self.result.stderr.replace("Error: No such object: biocontainers/openms:2.2.0_cv5", "")
                        # Generating the intermedite reports for Junit, also ignoring previous error
                        if "error" in standard_error_report.lower():
                            print(f'Standard Error:\n---------------------\n {self.result.stderr}')
                            print(f'Validation Report:\n---------------------\n{file_name} ::-->FAIL')
                            os.chdir(os.path.join(main_path, validation_path))
                            with open(tmp_error_report, 'a') as f:
                                f.write('\nfailure')
                                f.write(f'\n{diff_time}')
                            tmp_error_report2 = file_name + "_stderr_report.txt"
                            with open(tmp_error_report2, "w") as f:
                                f.write(self.result.stderr)
                            os.chdir(main_path)
                        else:
                            print(f'Validation Report:\n---------------------\n{file_name} ::-->SUCCESS')
                            os.chdir(os.path.join(main_path, validation_path))
                            with open(tmp_error_report, 'a') as f:
                                f.write('\nsuccess')
                                f.write(f'\n{diff_time}')
                            os.chdir(main_path)
                       
                        os.remove(bash_file_name)



test = validation_stage()
test.validation_process()
os.chdir(main_path)
